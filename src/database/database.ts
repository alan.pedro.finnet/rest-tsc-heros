import mongoose from'mongoose';

mongoose.connect('mongodb://mongo/heros', {/*mongo docker*/

    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
    
}).then(db => console.log('Database is running now'))
  .then( err => console.log(err));