 import { Router } from 'express';
 import { signup, signin, profile } from '../controllers/auth.controller';
 import { TokenValidation } from '../libs/verifyToken';

 //routes
 const route = Router();

 route.post('/signup', signup);
 route.post('/signin', signin);
 route.get('/profile', TokenValidation, profile);


 export default route;