import dotenv from 'dotenv';
import app from './app';
import serverless from 'serverless-http';
import './database/database';

dotenv.config();

app.listen(app.get('port'), () => {
    console.log(app.get('port'),);
});

module.exports.handler = serverless(app);