import express, { Application } from 'express';
import authRoute from './routes/auth';
import morgan from 'morgan';

const app = express();
const port = process.env.PORT || 8081;/* || 8081*/

//settings
app.set('port', port);

//middleware
app.use(express.json());
app.use(morgan('dev'));

//routes
app.use(authRoute);
export default app;
