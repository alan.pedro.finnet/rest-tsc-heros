import { Request, Response } from 'express';
import User, { IUser } from '../model/User';
import jwt from 'jsonwebtoken';

export const signup = async (req: Request, res: Response) => {    

    const user: IUser = new User({

        username: req.body.username,
        email: req.body.email,
        password: req.body.password

    });

    user.password = await user.encryptPassword(user.password);

    const userSaved = await user.save();

    const token: string = jwt.sign({_id: userSaved._id }, process.env.TOKEN_SECRET || 'tokentest');

    res.header('auth-token', token).json({ userSaved });
}

export const signin = async (req: Request, res: Response) => {

    const newUser = await User.findOne({email: req.body.email});

    if(!newUser) return res.status(400).json({message: 'User or password wrong'});

    const correctPassword: boolean = await newUser.validatePassword(req.body.password);
    if(!correctPassword) return res.status(400).json({message:'Password wrong'});

    const token: string = jwt.sign({_id: newUser._id}, process.env.TOKEN_SECRET || 'tokentest', {
        expiresIn: 60 * 60 /*60 * 60 * 24 */
    });

    res.header('auth-token', token).json(newUser);
}

export const profile = async (req: Request, res: Response) => {
    const user = await User.findById(req.userId, {password: 0});

    if(!user) return res.status(404).json('No user found')
    res.json({data: user});
}