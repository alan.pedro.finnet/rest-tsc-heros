"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const app_1 = __importDefault(require("./app"));
const serverless_http_1 = __importDefault(require("serverless-http"));
require("./database/database");
dotenv_1.default.config();
app_1.default.listen(app_1.default.get('port'), () => {
    console.log(app_1.default.get('port'));
});
module.exports.handler = serverless_http_1.default(app_1.default);
//# sourceMappingURL=index.js.map