"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_controller_1 = require("../controllers/auth.controller");
const verifyToken_1 = require("../libs/verifyToken");
//routes
const route = express_1.Router();
route.post('/signup', auth_controller_1.signup);
route.post('/signin', auth_controller_1.signin);
route.get('/profile', verifyToken_1.TokenValidation, auth_controller_1.profile);
exports.default = route;
//# sourceMappingURL=auth.js.map